# ==USE AT YOUR OWN RISK==
# JustBrowsing
A simple Starsector combat plugin/mod/cheat. When transferring control to a ship with an officer, the ship retains the officer's skills.

Provided is a .zip package containing the mod, including a signed .jar with the plugin itself.

<center>![ex2](examples/example_boarding.gif)</center>

## How it works
It's an every-frame combat plugin, actually executing for every 90th frame. 

- When transferring between ships: sets the shuttle captain to the player and reassignes the departed ship's original Officer (*the ship's Officer tends to be set as the one transferring by shuttlepod, essentially leaving their ship*)

- When boarding a ship with an assigned Officer, reassignes the Officer as the captain.

**Not every scenario has been tested**
